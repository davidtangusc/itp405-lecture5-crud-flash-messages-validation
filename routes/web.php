<?php

Route::get('/', function () {
    return redirect('/songs');
});

Route::get('/songs', 'SongController@index');
Route::get('/songs/create', 'SongController@create');
Route::post('/songs', 'SongController@store');
Route::get('/songs/{id}/delete', 'SongController@destroy');
Route::get('/songs/{id}/edit', 'SongController@edit');
Route::post('/songs/{id}/edit', 'SongController@update');
