<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Songs</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h1>Create a Song</h1>

          @if (count($errors) > 0)
            <div class="alert alert-danger">
              <ul>
                @foreach($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
          @endif

          <form action="/songs" method="post">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="title">Song Title</label>
              <input type="text" name="title" class="form-control" id="title">
            </div>
            <div class="form-group">
              <label for="price">Price</label>
              <input type="text" name="price" class="form-control" id="price">
            </div>
            <div class="form-group">
              <label for="artist">Artist</label>
              <select class="form-control" name="artist" id="artist">
                @foreach($artists as $artist)
                  <option value="{{ $artist->id }}">{{ $artist->artist_name }}</option>
                @endforeach
              </select>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>
