<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Songs</title>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col-xs-12">
          @if (session('successStatus'))
            <div class="alert alert-success" role="alert">
              {{ session('successStatus') }}
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <a href="/songs/create" class="btn">Add a Song</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <table class="table table-striped">
            <thead>
              <tr>
                <th>Song Title</th>
                <th>Artist</th>
                <th colspan="2">Price</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($songs as $song)
                <tr>
                  <td>{{ $song->title }}</td>
                  <td>{{ $song->artist_name }}</td>
                  <td>${{ $song->price }}</td>
                  <td>
                    <a href="/songs/{{ $song->id }}/delete">Delete</a>
                    <span>|</span>
                    <a href="/songs/{{ $song->id }}/edit">Edit</a>
                  </td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </body>
</html>
