<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;

class SongController extends Controller
{
    public function index()
    {
        $songs = DB::table('songs')
            ->select('songs.id', 'artist_name', 'title', 'price')
            ->join('artists', 'songs.artist_id', '=', 'artists.id')
            ->orderBy('artist_name')
            ->get();

        return view('songs.index', [
            'songs' => $songs
        ]);
    }

    public function create()
    {
        $artists = DB::table('artists')
            ->select('id', 'artist_name')
            ->orderBy('artist_name')
            ->get();

        return view('songs.create', [
            'artists' => $artists
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price' => 'required|numeric'
        ]);

        if ($validator->passes()) {
            DB::table('songs')->insert([
                'title' => request('title'),
                'artist_id' => request('artist'),
                'price' => request('price')
            ]);

            return redirect('/songs')
                ->with('successStatus', 'Song created successfully!');
        } else {
            return redirect('/songs/create')->withErrors($validator);
        }
    }

    public function destroy($songID)
    {
        DB::table('songs')
            ->where('id', '=', $songID)
            ->delete();

        return redirect('/songs')
            ->with('successStatus', 'Song deleted successfully!');
    }

    public function edit($songID)
    {
        $song = DB::table('songs')
            ->where('id', '=', $songID)
            ->first();

        $artists = DB::table('artists')
            ->select('id', 'artist_name')
            ->orderBy('artist_name')
            ->get();

        return view('songs.edit', [
            'artists' => $artists,
            'song' => $song
        ]);
    }

    public function update($songID, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'price' => 'required|numeric'
        ]);

        if ($validator->passes()) {
            DB::table('songs')
                ->where('id', '=', $songID)
                ->update([
                    'title' => request('title'),
                    'artist_id' => request('artist'),
                    'price' => request('price')
                ]);

            return redirect('/songs')
                ->with('successStatus', 'Song updated successfully!');
        } else {
            return redirect("/songs/$songID/edit")
                ->withErrors($validator);
        }
    }
}
